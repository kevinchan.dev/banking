import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/core/customer.service';
import { AccountService } from 'src/app/core/account.service';
import { ApiService } from 'src/app/core/api.service';
import { Customer } from 'src/app/core/customer';
import { Account } from 'src/app/core/account';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  customer: Customer;
  balance: number;
  accounts: Account[];

  constructor(private customerService: CustomerService, private accountService: AccountService, private apiService: ApiService) { }

  ngOnInit() {
    this.customer = this.customerService.getCustomer(this.apiService.getCustomer());
    this.balance = this.accountService.getBalanceTotal(this.customer.id);
    this.accounts = this.accountService.getAccounts(this.customer.id);
  }

}
