import { Component, OnInit } from '@angular/core';
import { Account } from 'src/app/core/account';
import { Customer } from 'src/app/core/customer';
import { ApiService } from 'src/app/core/api.service';
import { AccountService } from 'src/app/core/account.service';
import { CustomerService } from 'src/app/core/customer.service';
import { MatDialog } from '@angular/material/dialog';
import { EditAccountComponent } from 'src/app/shared/edit-account/edit-account.component';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {

  customer: Customer;
  accounts: Account[];

  constructor(private customerService: CustomerService,
              private accountService: AccountService,
              private apiService: ApiService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.customer = this.customerService.getCustomer(this.apiService.getCustomer());
    this.accounts = this.accountService.getAccounts(this.customer.id);
  }

  onAdd() {
    const dialogRef = this.dialog.open(EditAccountComponent, {
      width: '350px',
      data: {
        account: {
          id: 0,
          email: '',
          password: '',
          fullName: '',
          parent: this.customer.id,
          balance: 0
        },
        action: 'Add'
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.accounts = this.accountService.getAccounts(this.customer.id);
    });
  }
  onEdit(id: number) {
    const dialogRef = this.dialog.open(EditAccountComponent, {
      width: '350px',
      data: {
        account: this.accounts.find(account => (account.id === id)),
        action: 'Edit'
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.accounts = this.accountService.getAccounts(this.customer.id);
    });
  }

  onDelete(id: number) {
    this.accountService.removeAccount(id);
    this.accounts = this.accountService.getAccounts(this.customer.id);
  }

}
