import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargeWithdrawComponent } from './charge-withdraw.component';
import { MatDialogModule } from '@angular/material/dialog';

describe('ChargeWithdrawComponent', () => {
  let component: ChargeWithdrawComponent;
  let fixture: ComponentFixture<ChargeWithdrawComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ MatDialogModule ],
      declarations: [ ChargeWithdrawComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargeWithdrawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
