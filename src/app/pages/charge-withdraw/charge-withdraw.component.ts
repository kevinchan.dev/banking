import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/api.service';
import { MatDialog } from '@angular/material/dialog';
import { TransactionComponent } from 'src/app/shared/transaction/transaction.component';

@Component({
  selector: 'app-charge-withdraw',
  templateUrl: './charge-withdraw.component.html',
  styleUrls: ['./charge-withdraw.component.scss']
})
export class ChargeWithdrawComponent implements OnInit {

  customer: number;

  constructor(private apiService: ApiService, public dialog: MatDialog) { }

  ngOnInit() {
    this.customer = this.apiService.getCustomer();
  }

  onCharge() {
    this.dialog.open(TransactionComponent, {
      width: '250px',
      data: {
        customer: this.customer,
        action: 'Charge'
      }
    });
  }

  onWithdraw() {
    this.dialog.open(TransactionComponent, {
      width: '250px',
      data: {
        customer: this.customer,
        action: 'Withdraw'
      }
    });
  }
}
