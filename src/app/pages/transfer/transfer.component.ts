import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/api.service';
import { MatDialog } from '@angular/material/dialog';
import { TransactionComponent } from 'src/app/shared/transaction/transaction.component';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss']
})
export class TransferComponent implements OnInit {

  customer: number;

  constructor(private apiService: ApiService, public dialog: MatDialog) { }

  ngOnInit() {
    this.customer = this.apiService.getCustomer();
  }

  onTransfer() {
    this.dialog.open(TransactionComponent, {
      width: '250px',
      data: {
        customer: this.customer,
        action: 'Transfer'
      }
    });
  }

}
