import { Injectable } from '@angular/core';
import { History } from './history';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {
  history: History[];

  constructor() {
    const localData = window.localStorage.getItem('history');
    if (localData === null) {
      this.history = [];
      window.localStorage.setItem('history', JSON.stringify(this.history));
    } else {
      this.history = JSON.parse(localData);
    }
  }

  getHistory(account: number) {
    return this.history.filter(item => (item.account === account)).sort((a, b) => (b.id - a.id));
  }

  addHistory(newHistory: History) {
    const newHistoryId: number = this.history.reduce((maxid, item) => (maxid > item.id ? maxid : item.id), 0);
    newHistory.id = newHistoryId + 1;
    this.history.push(newHistory);
    window.localStorage.setItem('history', JSON.stringify(this.history));
    return true;
  }
}
