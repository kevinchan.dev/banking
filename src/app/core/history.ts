export enum HistoryActionType {
  Charge = 'Charge',
  Withdraw = 'Withdraw',
  Send = 'Send',
  Receive = 'Receive'
}

export interface History {
  id: number;
  actionType: HistoryActionType;
  account: number;
  relatedInfo: number;
  dateTime: string;
  amount: number;
}
