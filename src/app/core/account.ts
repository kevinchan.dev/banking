export interface Account {
  id: number;
  email: string;
  fullName: string;
  password: string;
  parent: number;
  balance: number;
  joint?: number;
}
