import { Injectable } from '@angular/core';
import { Account } from './account';
import { accountsData } from './initial-data';
import { JointsService } from './joints.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  accounts: Account[];

  constructor(private jointsService: JointsService) {
    const localData = window.localStorage.getItem('accounts');
    if (localData === null) {
      this.accounts = accountsData;
      window.localStorage.setItem('accounts', JSON.stringify(this.accounts));
    } else {
      this.accounts = JSON.parse(localData);
    }
  }

  getAccounts(customer: number = 0) {
    if (customer === 0) {
      return this.accounts;
    } else {
      return this.accounts.filter(account => (account.parent === customer));
    }
  }
  getAccount(accountId: number) {
    return this.accounts.find(account => (account.id === accountId));
  }

  addAccount(newAccount: Account) {
    const newAccountId: number = this.accounts.reduce((maxid, account) => (maxid > account.id ? maxid : account.id), 0);
    newAccount.id = newAccountId + 1;
    this.accounts.push(newAccount);
    window.localStorage.setItem('accounts', JSON.stringify(this.accounts));
    return true;
  }
  updateAccount(newAccount: Account) {
    const selected: number = this.accounts.findIndex(account => (account.id === newAccount.id));
    if (selected === -1) {
      return false;
    } else {
      this.accounts[selected] = newAccount;
      window.localStorage.setItem('accounts', JSON.stringify(this.accounts));
      return true;
    }
  }
  removeAccount(accountId: number) {
    const selected: number = this.accounts.findIndex(account => (account.id === accountId));
    if (selected === -1) {
      return false;
    } else {
      this.accounts.splice(selected, 1);
      window.localStorage.setItem('accounts', JSON.stringify(this.accounts));
      return true;
    }
  }

  changeBalance(accountId: number, amount: number) {
    const selected: number = this.accounts.findIndex(account => (account.id === accountId));
    if (selected === -1) {
      return false;
    } else {
      if (this.accounts[selected].joint > 0) {
        const joints = this.jointsService.getJointAccounts(this.accounts[selected].joint);
        joints.forEach(item => (this.accounts.find(account => (account.id === item)).balance += amount));
      } else {
        this.accounts[selected].balance += amount;
      }
      window.localStorage.setItem('accounts', JSON.stringify(this.accounts));
      return true;
    }
  }
  getBalanceTotal(customerId: number) {
    return this.accounts.filter(account => (account.parent === customerId)).reduce((total, account) => (total + account.balance), 0);
  }
}
