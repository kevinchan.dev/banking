import { Account } from './account';
import { Customer } from './customer';
import { Joints } from './joints';

export const accountsData: Account[] = [
  {
    id: 1,
    email: 'account1@bank.com',
    fullName: 'Account1',
    password: 'Account1',
    parent: 1,
    balance: 0
  }, {
    id: 2,
    email: 'account2@bank.com',
    fullName: 'Account2',
    password: 'Account2',
    parent: 1,
    joint: 1,
    balance: 0
  }, {
    id: 3,
    email: 'account3@bank.com',
    fullName: 'Account3',
    password: 'Account3',
    parent: 1,
    balance: 0
  }, {
    id: 4,
    email: 'account4@bank.com',
    fullName: 'Account4',
    password: 'Account4',
    parent: 2,
    balance: 0
  }, {
    id: 5,
    email: 'account5@bank.com',
    fullName: 'Account5',
    password: 'Account5',
    parent: 2,
    balance: 0
  }, {
    id: 6,
    email: 'account6@bank.com',
    fullName: 'Account6',
    password: 'Account6',
    parent: 2,
    joint: 1,
    balance: 0
  }
];

export const customersData: Customer[] = [
  {
    id: 1,
    userName: 'Customer1',
    email: 'customer1@bank.com'
  }, {
    id: 2,
    userName: 'Customer2',
    email: 'customer2@bank.com'
  }
];

export const jointData: Joints[] = [
  {
    id: 1,
    accounts: [2, 6]
  }
];
