import { Injectable } from '@angular/core';
import { Joints } from './joints';
import { jointData } from './initial-data';

@Injectable({
  providedIn: 'root'
})
export class JointsService {
  joints: Joints[];
  constructor() {
    const localData = window.localStorage.getItem('joints');
    if (localData === null) {
      this.joints = jointData;
      window.localStorage.setItem('joints', JSON.stringify(this.joints));
    } else {
      this.joints = JSON.parse(localData);
    }
  }

  getJointAccounts(id: number) {
    const found = this.joints.find(item => (item.id === id));
    console.log(this.joints);
    if (found === undefined) {
      return [];
    } else {
      return this.joints.find(item => (item.id === id)).accounts;
    }
  }
}
