import { Injectable } from '@angular/core';
import { HistoryService } from './history.service';
import { AccountService } from './account.service';
import { History, HistoryActionType } from './history';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  customer: number;
  constructor(private historyService: HistoryService, private accountService: AccountService) {
    const localData = window.localStorage.getItem('customer');
    if (localData === null) {
      this.customer = 1;
      window.localStorage.setItem('customer', JSON.stringify(this.customer));
    } else {
      this.customer = parseInt(localData, 10);
    }
  }

  getCustomer(): number {
    return this.customer;
  }
  setCustomer(customer: number) {
    this.customer = customer;
    window.localStorage.setItem('customer', JSON.stringify(this.customer));
  }

  charge(account: number, amount: number) {
    const now = new Date();
    const dateString =
      `${now.getFullYear()}-${(now.getMonth() + 1)}-${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`;
    const accountInfo = this.accountService.getAccount(account);
    const history: History = {
      id: 0,
      actionType: HistoryActionType.Charge,
      account,
      relatedInfo: 0,
      amount,
      dateTime: dateString
    };
    this.historyService.addHistory(history);
    this.accountService.changeBalance(account, amount);
    return true;
  }
  withdraw(account: number, amount: number) {
    const now = new Date();
    const dateString =
      `${now.getFullYear()}-${(now.getMonth() + 1)}-${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`;
    const accountInfo = this.accountService.getAccount(account);
    const history: History = {
      id: 0,
      actionType: HistoryActionType.Withdraw,
      account,
      relatedInfo: 0,
      amount,
      dateTime: dateString
    };
    if (accountInfo.balance < amount) {
      return false;
    } else {
      this.accountService.changeBalance(account, -amount);
      this.historyService.addHistory(history);
      return true;
    }
  }
  transfer(fromAccount: number, toAccount: number, amount: number) {
    const now = new Date();
    const dateString =
      `${now.getFullYear()}-${(now.getMonth() + 1)}-${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`;
    const sender = this.accountService.getAccount(fromAccount);
    const receiver = this.accountService.getAccount(toAccount);
    const senderHistory: History = {
      id: 0,
      actionType: HistoryActionType.Send,
      account: fromAccount,
      relatedInfo: toAccount,
      amount,
      dateTime: dateString
    };
    const receiverHistory: History = {
      id: 0,
      actionType: HistoryActionType.Receive,
      account: toAccount,
      relatedInfo: fromAccount,
      amount,
      dateTime: dateString
    };

    if (sender.balance < amount) {
      return false;
    } else {
      this.historyService.addHistory(senderHistory);
      this.historyService.addHistory(receiverHistory);
      this.accountService.changeBalance(sender.id, -amount);
      this.accountService.changeBalance(receiver.id, amount);
      return true;
    }
  }
}
