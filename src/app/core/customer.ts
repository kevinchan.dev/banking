export interface Customer {
  id: number;
  userName: string;
  email: string;
}
