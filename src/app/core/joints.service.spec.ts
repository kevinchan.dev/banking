import { TestBed } from '@angular/core/testing';

import { JointsService } from './joints.service';

describe('JointsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JointsService = TestBed.get(JointsService);
    expect(service).toBeTruthy();
  });
});
