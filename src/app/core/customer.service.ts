import { Injectable } from '@angular/core';
import { Customer } from './customer';
import { customersData } from './initial-data';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  customers: Customer[];

  constructor() {
    const localData = window.localStorage.getItem('customers');
    if (localData === null) {
      this.customers = customersData;
      window.localStorage.setItem('customers', JSON.stringify(this.customers));
    } else {
      this.customers = JSON.parse(localData);
    }
  }

  getCustomers() {
    return this.customers;
  }
  getCustomer(customerId: number) {
    return this.customers.find(customer => (customer.id === customerId));
  }

  addCustomer(newCustomer: Customer) {
    const newCustomerId: number = this.customers.reduce((maxid, customer) => (maxid > customer.id ? maxid : customer.id), 0);
    newCustomer.id = newCustomerId + 1;
    this.customers.push(newCustomer);
    window.localStorage.setItem('customers', JSON.stringify(this.customers));
    return true;
  }
  updateCustomer(newCustomer: Customer) {
    const selected: number = this.customers.findIndex(customer => (customer.id === newCustomer.id));
    if (selected === -1) {
      return false;
    } else {
      this.customers[selected] = newCustomer;
      window.localStorage.setItem('customers', JSON.stringify(this.customers));
      return true;
    }
  }
  removeCustomer(customerId: number) {
    const selected: number = this.customers.findIndex(customer => (customer.id === customerId));
    if (selected === -1) {
      return false;
    } else {
      this.customers.splice(selected, 1);
      window.localStorage.setItem('customers', JSON.stringify(this.customers));
      return true;
    }
  }
}
