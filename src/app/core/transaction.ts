export interface Transaction {
  customer: number;
  action: string;
  account: number;
  fromAccount: number;
  toAccount: number;
  amount: number;
}
