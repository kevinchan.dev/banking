import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ChargeWithdrawComponent } from './pages/charge-withdraw/charge-withdraw.component';
import { AccountsComponent } from './pages/accounts/accounts.component';
import { TransferComponent } from './pages/transfer/transfer.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  }, {
    path: 'charge-withdraw',
    component: ChargeWithdrawComponent
  }, {
    path: 'accounts',
    component: AccountsComponent
  }, {
    path: 'transfer',
    component: TransferComponent
  }, {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
