import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareAccountComponent } from './share-account.component';

describe('ShareAccountComponent', () => {
  let component: ShareAccountComponent;
  let fixture: ComponentFixture<ShareAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
