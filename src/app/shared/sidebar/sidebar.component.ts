import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  urlList = [
    {
      url: '/dashboard',
      title: 'Dashboard'
    }, {
      url: '/accounts',
      title: 'Accounts'
    }, {
      url: '/charge-withdraw',
      title: 'Charge / Withdraw'
    }, {
      url: '/transfer',
      title: 'Transfer Money'
    }
  ];
  currentUrl: string;
  constructor(private router: Router) { }

  ngOnInit() {
  }

}
