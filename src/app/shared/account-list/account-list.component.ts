import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { Account } from 'src/app/core/account';
import { Customer } from 'src/app/core/customer';
import { CustomerService } from 'src/app/core/customer.service';
import { AccountService } from 'src/app/core/account.service';

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.scss']
})
export class AccountListComponent implements OnInit {

  accountControl = new FormControl('', [Validators.required]);
  @Output() selectionChanged: EventEmitter<any> = new EventEmitter();

  @Input() customer = 0;
  @Input() label = 'Choose An Account';
  accounts: Account[];

  constructor(private customerService: CustomerService, private accountService: AccountService) {}

  ngOnInit() {
    this.accounts = this.accountService.getAccounts(this.customer);
  }

  onSelectionChanged(ev: any) {
    this.selectionChanged.emit(ev.value);
  }

}
