import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Transaction } from 'src/app/core/transaction';
import { Account } from 'src/app/core/account';
import { ApiService } from 'src/app/core/api.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<TransactionComponent>, @Inject(MAT_DIALOG_DATA) public data: Transaction, private apiService: ApiService) { }

  message: string;
  hasError: boolean = false;

  ngOnInit() {
    this.data.amount = 100;
  }

  onAccountChange(ev: Account) {
    this.data.account = ev.id;
  }
  onFromAccountChange(ev: Account) {
    this.data.fromAccount = ev.id;
  }
  onToAccountChange(ev: Account) {
    this.data.toAccount = ev.id;
  }
  onAmountChange(ev: number) {
    this.data.amount = ev;
  }

  onNoClick() {
    this.dialogRef.close();
  }

  onPerform() {
    if (this.data.amount === undefined || this.data.account === undefined) {
      return;
    } else if (this.data.amount < 100) {
      this.hasError = true;
      this.message = 'The minium transaction amount is 100.';
      return;
    } else {
      switch (this.data.action) {
        case 'Charge':
          if (this.apiService.charge(this.data.account, this.data.amount)) {
            this.hasError = false;
          } else {
            this.hasError = true;
            this.message = 'The Charge Transaction failed.';
          }
          break;
        case 'Withdraw':
          if (this.apiService.withdraw(this.data.account, this.data.amount)) {
            this.hasError = false;
          } else {
            this.hasError = true;
            this.message = 'The Withdraw Transaction failed.';
          }
          break;
        case 'Transfer':
          if (this.apiService.transfer(this.data.fromAccount, this.data.toAccount, this.data.amount)) {
            this.hasError = false;
          } else {
            this.hasError = true;
            this.message = 'The Transaction Transaction failed.';
          }
          break;
      }
    }
    if (this.hasError === false) {
      this.dialogRef.close();
    }
  }
}
