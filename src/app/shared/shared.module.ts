import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';

import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { CustomerInfoComponent } from './customer-info/customer-info.component';
import { RouterModule } from '@angular/router';
import { AccountListComponent } from './account-list/account-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AmountInputComponent } from './amount-input/amount-input.component';
import { TransactionComponent } from './transaction/transaction.component';
import { MatDialogModule } from '@angular/material/dialog';
import { EditAccountComponent } from './edit-account/edit-account.component';
import { ShareAccountComponent } from './share-account/share-account.component';
import { SwitchCustomerComponent } from './switch-customer/switch-customer.component';

@NgModule({
  declarations: [
    HeaderComponent,
    SidebarComponent,
    CustomerInfoComponent,
    AccountListComponent,
    AmountInputComponent,
    TransactionComponent,
    EditAccountComponent,
    ShareAccountComponent,
    SwitchCustomerComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatSelectModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatDialogModule
  ],
  exports: [
    HeaderComponent,
    SidebarComponent,
    CustomerInfoComponent,
    AccountListComponent,
    AmountInputComponent,
    MatButtonModule
  ],
  entryComponents: [
    TransactionComponent,
    EditAccountComponent,
    ShareAccountComponent,
    SwitchCustomerComponent
  ]
})
export class SharedModule { }
