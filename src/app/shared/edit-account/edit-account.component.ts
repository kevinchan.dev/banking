import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Account } from 'src/app/core/account';
import { FormControl, Validators } from '@angular/forms';
import { AccountService } from 'src/app/core/account.service';

export interface AccountParam {
  account: Account;
  action: string;
}

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.component.html',
  styleUrls: ['./edit-account.component.scss']
})
export class EditAccountComponent implements OnInit {

  message: string;
  hasError: boolean = false;

  emailControl = new FormControl('', [
    Validators.required,
    Validators.email
  ]);
  nameControl = new FormControl('', [Validators.required]);
  passwordControl = new FormControl('', [Validators.required]);

  accountData: Account;

  constructor(public dialogRef: MatDialogRef<EditAccountComponent>,
              private accountService: AccountService,
              @Inject(MAT_DIALOG_DATA) public data: AccountParam) { }

  ngOnInit() {
    this.accountData = JSON.parse(JSON.stringify(this.data.account));
  }

  onNoClick() {
    this.dialogRef.close();
  }
  onPerform() {
    if (this.emailControl.hasError('email') ||
      this.emailControl.hasError('required') ||
      this.nameControl.hasError('required') ||
      this.passwordControl.hasError('required')) {
        return;
    }
    switch (this.data.action) {
      case 'Add':
        this.accountService.addAccount(this.accountData);
        break;
      case 'Edit':
        this.accountService.updateAccount(this.accountData);
        break;
    }
    this.dialogRef.close();
  }

}
