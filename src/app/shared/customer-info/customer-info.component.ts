import { Component, OnInit, Input } from '@angular/core';
import { Customer } from 'src/app/core/customer';
import { AccountService } from 'src/app/core/account.service';
import { ApiService } from 'src/app/core/api.service';
import { CustomerService } from 'src/app/core/customer.service';
import { MatDialog } from '@angular/material/dialog';
import { SwitchCustomerComponent } from '../switch-customer/switch-customer.component';

@Component({
  selector: 'app-customer-info',
  templateUrl: './customer-info.component.html',
  styleUrls: ['./customer-info.component.scss']
})
export class CustomerInfoComponent implements OnInit {

  customer: Customer;
  balance: number;

  constructor(private accountService: AccountService,
              private apiService: ApiService,
              private customerService: CustomerService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.customer = this.customerService.getCustomer(this.apiService.getCustomer());
  }

  onSwitchCustomer() {
    const dialogRef = this.dialog.open(SwitchCustomerComponent, {
      width: '250px'
    });
    dialogRef.afterClosed().subscribe(() => {
      this.customer = this.customerService.getCustomer(this.apiService.getCustomer());
    });
  }
}
