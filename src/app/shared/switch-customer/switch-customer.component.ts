import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { ApiService } from 'src/app/core/api.service';
import { Customer } from 'src/app/core/customer';
import { CustomerService } from 'src/app/core/customer.service';

@Component({
  selector: 'app-switch-customer',
  templateUrl: './switch-customer.component.html',
  styleUrls: ['./switch-customer.component.scss']
})
export class SwitchCustomerComponent implements OnInit {
  customerControl = new FormControl('', [Validators.required]);
  customers: Customer[];
  customer: number = 0;
  constructor(public dialogRef: MatDialogRef<SwitchCustomerComponent>,
              private apiService: ApiService, private customerService: CustomerService) { }

  ngOnInit() {
    this.customers = this.customerService.getCustomers();
  }

  onSelectionChanged(ev: any) {
    this.customer = ev.value.id;
  }

  onNoClick() {
    this.dialogRef.close();
  }

  onPerform() {
    this.apiService.setCustomer(this.customer);
    this.dialogRef.close();
  }
}
