import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchCustomerComponent } from './switch-customer.component';
import { SharedModule } from '../shared.module';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('SwitchCustomerComponent', () => {
  let component: SwitchCustomerComponent;
  let fixture: ComponentFixture<SwitchCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ SharedModule, MatDialogModule, BrowserAnimationsModule ],
      declarations: [ ],
      providers: [ {provide: MatDialogRef, useValue: {}} ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
